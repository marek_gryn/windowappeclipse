package eclipse.beta.eclipse.beta;

import java.awt.EventQueue;

import javax.swing.JFrame;
import java.awt.GridBagLayout;
import javax.swing.JPanel;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.GridLayout;
import javax.swing.border.LineBorder;
import java.awt.Color;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;

public class Window {

	private JFrame frame;
	private JTextField textTitle;
	private JTextField textRezyser;
	private JTextField textScore;
	private JTextField textReleaseDate;
	private JTextField textCategory;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Window window = new Window();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Window() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[]{0, 0};
		gridBagLayout.rowHeights = new int[]{0, 0};
		gridBagLayout.columnWeights = new double[]{1.0, Double.MIN_VALUE};
		gridBagLayout.rowWeights = new double[]{1.0, Double.MIN_VALUE};
		frame.getContentPane().setLayout(gridBagLayout);
		
		JPanel panel = new JPanel();
		GridBagConstraints gbc_panel = new GridBagConstraints();
		gbc_panel.fill = GridBagConstraints.BOTH;
		gbc_panel.gridx = 0;
		gbc_panel.gridy = 0;
		frame.getContentPane().add(panel, gbc_panel);
		panel.setLayout(new GridLayout(0, 2, 0, 0));
		
		JPanel panelLeft = new JPanel();
		panelLeft.setBorder(new LineBorder(new Color(0, 0, 0)));
		panel.add(panelLeft);
		panelLeft.setLayout(new GridLayout(0, 1, 0, 0));
		
		JPanel panelLeftTop = new JPanel();
		panelLeft.add(panelLeftTop);
		panelLeftTop.setLayout(new GridLayout(0, 1, 0, 0));
		
		JLabel lblNewLabel = new JLabel("Dane filmu");
		panelLeftTop.add(lblNewLabel);
		
		JPanel panelTitle = new JPanel();
		panelLeftTop.add(panelTitle);
		panelTitle.setLayout(new GridLayout(1, 0, 0, 0));
		
		JLabel title = new JLabel("Title");
		panelTitle.add(title);
		
		textTitle = new JTextField();
		panelTitle.add(textTitle);
		textTitle.setColumns(10);
		
		JPanel panelRezyser = new JPanel();
		panelLeftTop.add(panelRezyser);
		panelRezyser.setLayout(new GridLayout(1, 0, 0, 0));
		
		JLabel lblNewLabel_1 = new JLabel("Rezyser");
		panelRezyser.add(lblNewLabel_1);
		
		textRezyser = new JTextField();
		panelRezyser.add(textRezyser);
		textRezyser.setColumns(10);
		
		JPanel panelReleaseDate = new JPanel();
		panelLeftTop.add(panelReleaseDate);
		panelReleaseDate.setLayout(new GridLayout(1, 0, 0, 0));
		
		JLabel lblNewLabel_3 = new JLabel("Release date");
		panelReleaseDate.add(lblNewLabel_3);
		
		textReleaseDate = new JTextField();
		panelReleaseDate.add(textReleaseDate);
		textReleaseDate.setColumns(10);
		
		JPanel panelCategory = new JPanel();
		panelLeftTop.add(panelCategory);
		panelCategory.setLayout(new GridLayout(1, 0, 0, 0));
		
		JLabel lblNewLabel_4 = new JLabel("Category");
		panelCategory.add(lblNewLabel_4);
		
		textCategory = new JTextField();
		panelCategory.add(textCategory);
		textCategory.setColumns(10);
		
		JPanel panelScore = new JPanel();
		panelLeftTop.add(panelScore);
		panelScore.setLayout(new GridLayout(1, 0, 0, 0));
		
		JLabel lblNewLabel_2 = new JLabel("Score");
		panelScore.add(lblNewLabel_2);
		
		textScore = new JTextField();
		panelScore.add(textScore);
		textScore.setColumns(10);
		
		JPanel panelLeftBottom = new JPanel();
		panelLeft.add(panelLeftBottom);
		panelLeftBottom.setLayout(new GridLayout(0, 1, 0, 0));
		
		JButton btnAdd = new JButton("Add");
		panelLeftBottom.add(btnAdd);
		
		JButton btnDelete = new JButton("Delete");
		panelLeftBottom.add(btnDelete);
		
		JButton btnNewButton_2 = new JButton("New button");
		panelLeftBottom.add(btnNewButton_2);
		
		JButton btnNewButton_3 = new JButton("New button");
		panelLeftBottom.add(btnNewButton_3);
		
		JPanel panelRight = new JPanel();
		panelRight.setBorder(new LineBorder(new Color(0, 0, 0)));
		panel.add(panelRight);
		panelRight.setLayout(new GridLayout(1, 0, 0, 0));
	}

}
